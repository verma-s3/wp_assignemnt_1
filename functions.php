<?php

//to register the navigational menu
register_nav_menus();

//to check if this funciton is not existed then loaded that fucntion to run funcitonality
if(!function_exists('load_wp_scripts')){
	
	function load_wp_scripts()
	{
		wp_enqueue_style('bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css');
		wp_enqueue_style('wp_assignment_1',get_stylesheet_uri(),['bootstrap']);
		
		wp_enqueue_script('bootstrap','https//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js',['jquery'],null,true);
	}
	add_action('wp_enqueue_scripts','load_wp_scripts',10);
}


//fucntions for navigational menu

// for util navigation
if(!function_exists('westland_util_menu'))
{
	function westland_util_menu(){
		wp_nav_menu(['menu'=>'util','container'=>'false']);
	}
}
// for main navigation
if(!function_exists('westland_main_menu'))
{
	function westland_main_menu(){
		wp_nav_menu(['menu'=>'main','container'=>'false']);
	}
}
// for footer navigation
if(!function_exists('westland_footer1_menu'))
{
	function westland_footer1_menu(){
		wp_nav_menu(['menu'=>'footer1','container'=>'false']);
	}
}
// for footer navigation
if(!function_exists('westland_footer2_menu'))
{
	function westland_footer2_menu(){
		wp_nav_menu(['menu'=>'footer2','container'=>'false']);
	}
}

// add support for featured image
add_theme_support('post-thumbnails');

// to chage dots to lear nore text
function excerpt_readmore($more) {
return '... <a href="'. get_permalink($post->ID) . '" class="readmore">' . 'Learn More >>' . '</a>';
}
add_filter('excerpt_more', 'excerpt_readmore');