<?php get_header(); ?>

		<div id="content" class="col-xs-12">
			<div id="primary" class="col-xs-12">
				<?php if(have_posts()):?>
					<!-- the loop for post iteration -->
				<?php while(have_posts()): the_post(); ?>
					 <article>
					 	<h1 class="title"><?php the_title() ?></h1>
						<p><?php the_content(); ?></p>
					</article>
				<?php endwhile; ?>
				<?php endif; ?>
			</div><!-- /primary -->
		</div><!-- /content -->

<?php get_footer(); ?>