<!-- Footer page  -->
		<footer class="main col-xs-12">
			<div class="col-xs-2">
				<img src="<?=get_template_directory_uri()?>/images/Playful_dog.png" alt="Playful_dog" />
			</div>
			<div class="col-xs-8">
				<!-- footer nav menus from wordpress -->
				<nav id="footer">
					<?php westland_footer1_menu(); ?>
					<?php westland_footer2_menu(); ?>
				</nav>
				<p class="copyright">Contents copyright &copy; 2010 - 2014 by Westland Partners</p>
			</div>
			<div class="col-xs-2 wordmark">
				<img src="<?=get_template_directory_uri()?>/images/Wordmark.png" alt="Watermark" />
			</div>
		</footer>
		
	</div><!-- /container -->
	<?php wp_footer(); ?>
	</body>
</html>