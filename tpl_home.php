<?php get_header(); ?>
<!-- Template Name: Home Page -->
		<div id="content" class="col-xs-12">
			<div id="primary" class="col-xs-12">
				<div id="featured" class="col-xs-12">
					 <?php echo do_shortcode('[wonderplugin_slider id=1]'); ?> 
				</div><!-- /featured -->
			</div><!-- /primary -->

			<div id="secondary" class="col-xs-12 sub">

				<div id="callout_1" class="callout col-sm-4 col-xs-12">
					<div class="col-xs-12">
					<a href="#"><img src="<?=get_template_directory_uri()?>/images/vet_dog.png" alt="Vetrinarians with dog" /></a>
					<div class="caption  col-xs-12">
							<h4>For Veterinarians</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco.</p>
							<button>Read More ></button>	
					</div><!-- /caption -->
					</div>
				</div><!-- /callout -->
				
				<div id="callout_2" class="callout col-sm-4 col-xs-12">
				<div class="col-xs-12">
				<a href="#"><img src="<?=get_template_directory_uri()?>/images/Pet_owner.png" alt="Pet owner" /></a>
					<div class="caption  col-xs-12">
							<h4>For Pet Owners</h4>	
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco.</p>
							<button>Read More ></button>
					</div><!-- /caption -->
				</div>
				</div><!-- /callout -->
				
				<div id="callout_3" class="callout callout col-sm-4 col-xs-12">
					<div class="col-xs-12">
						<a href="#"><img src="<?=get_template_directory_uri()?>/images/Owners.png" alt="Owners" /></a>
						<div class="caption  col-xs-12">
								<h4>Commited to health</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco.</p>
							<button>Read More ></button>		
						</div><!-- /caption -->
					</div>
				</div><!-- /callout -->
				

			</div><!-- /secondary -->

		</div><!-- /content -->
<?php get_footer() ?>