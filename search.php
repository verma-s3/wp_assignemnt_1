<?php get_header(); ?>

		<div id="content" class="col-xs-12">
			<div id="primary" class="col-xs-12">
				<h2 style="margin-left:30px;">Search: <?php echo get_search_query() ?></h2>
				<?php if(have_posts()):?>
					<!-- loop to check posts iteration -->
				<?php while(have_posts()): the_post(); ?>
					 <article>

						<h2><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>

						<sapn><?php the_date() ?></sapn>
						<p><?php the_excerpt(); ?></p>

					</article>
				<?php endwhile; ?>
				<?php endif; ?>
	
			</div><!-- /primary -->

		<?php get_sidebar(); ?>

		</div><!-- /content -->

<?php get_footer(); ?>