<?php get_header(); ?>

		<div id="content" class="col-xs-12">
			<div id="main_sidebar" class="col-xs-3">
				<h3>Section menu</h3>
				<nav id="sidebar" style="margin-left: 10px;">
					<?php wp_list_categories(); ?>
				</nav>
			</div><!--main-slider div -->
			<div id="secondary" class="col-xs-9" style="border-left: 2px solid #cfcfcf">
				<h2>Prepared Food</h2>
				<h3><?=get_the_archive_title() ?></h3>
				<!-- check for have posts -->
				<?php if(have_posts()): ?>
					<?php if(is_category('cat-food')): ?>
						<!-- The Loop for posts  -->
						<?php while(have_posts()): the_post(); ?>
					 		
							<div class="callout_4 callout col-xs-12">
								<div class="col-xs-3 thumbnail">
									<?php the_post_thumbnail(); ?>
								</div>
							<div class="caption">
									<h4><a href="<?php the_permalink()?>"><?php the_title();?></a></h4>
									<?php the_excerpt(); ?>
									
							</div><!-- /caption -->
							
						</div><!-- /callout -->
						<?php endwhile; ?>
					<?php elseif(is_category('dog-food')) : ?>
						<?php while(have_posts()): the_post(); ?>
					 		
							<div class="callout_4 callout col-xs-12">
								<div class="col-xs-3 thumbnail">
									<?php the_post_thumbnail(); ?>
								</div>
							<div class="caption">
									<h4><a href="<?php the_permalink()?>"><?php the_title();?></a></h4>
									<?php the_excerpt(); ?>
									
							</div><!-- /caption -->
							
						</div><!-- /callout -->
						<?php endwhile; ?>
					<?php endif; ?>

				<?php endif;?>
				
								

			</div><!-- /secondary -->

		</div><!-- /content -->
<?php get_footer(); ?>