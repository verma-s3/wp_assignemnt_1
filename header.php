<!-- header fiel for index.php file -->
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Westland Pet Food</title>
	
	<?php wp_head(); ?>
</head>
<body class="home">
	<img src="<?=get_template_directory_uri()?>/images/facebook.png" alt="facebook-image" class="pic1" />
	<img src="<?=get_template_directory_uri()?>/images/skype.png" alt="twitter-image" class="pic2"/>
	<!-- utility navigation Manu -->
	<nav id="util">
		<?php westland_util_menu(); ?>
	</nav>
	<div class="container">
		<!-- header tag start -->
		<header class="main col-xs-12">
			<?php if(is_page('home')) : ?>
				<img src="<?=get_template_directory_uri()?>/images/head_main.png" alt="head_main" />
			<?php else : ?>
				<img src="<?=get_template_directory_uri()?>/images/header2.png" alt="header_image" />
			<?php endif; ?>
		</header>
		<!-- main nav menu  -->
		<nav id="main" class="col-xs-12">
			<?php westland_main_menu(); ?>
		</nav>