<?php get_header(); ?>

		<div id="content" class="col-xs-12">
			<div id="primary" class="col-xs-12">
				<article>
					<h1 class="archive_title">404 | NOT FOUND 🥴🥴🥴🥴</h1>
					<!-- messge foe page not found -->
					<p>Opps!!!! Unable to finds the page, you are looking for.</p>
					<h2>Try Searching for something else</h2>
					<div class="well">
						<?php get_search_form(); ?>
					</div>
				</article><!-- article ends -->
			</div><!-- /primary -->
		</div><!-- /content -->

<?php get_footer(); ?>